// AEDVICES (c) 2022
// François CERISIER
// francois@aedvices.com

/*
CeCILL-B License
The contents of this file are subject to the restrictions and limitations
set forth in the CeCILL-B License (the "License");
You may not use this file except in compliance with such restrictions and
limitations. You may obtain instructions on how to receive a copy of the
License at http://www.cecill.info . Software distributed by Contributors
under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
ANY KIND, either express or implied. See the License for the specific
language governing rights and limitations under the License.

*/


// Questa:
// vlog sudoku.sv
// vsim -solvefaildebug=2 -solvefailseverity=3 -c sudoku -do "run -all"


class sudoku_solver;
    // Original Grid
    int grid[9][9] = '{

        { 0, 0, 1, 0, 0, 9, 2, 0, 0 },
        { 6, 3, 0, 0, 0, 0, 0, 7, 0 },
        { 0, 0, 0, 0, 3, 0, 8, 5, 1 },
        { 0, 0, 6, 0, 0, 8, 0, 0, 7 },
        { 0, 8, 0, 0, 0, 0, 0, 4, 0 },
        { 9, 0, 0, 6, 0, 0, 5, 0, 0 },
        { 4, 2, 3, 0, 9, 0, 0, 0, 0 },
        { 0, 1, 0, 0, 0, 0, 0, 2, 5 },
        { 0, 0, 7, 0, 1, 0, 9, 0, 4 }

// Other examples
/*
        { 0, 3, 1, 0, 0, 0, 0, 0, 9 },
        { 6, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 9, 0, 3, 0, 8, 0, 1 },
        { 1, 0, 6, 0, 0, 8, 0, 0, 7 },
        { 0, 8, 0, 0, 0, 0, 0, 4, 0 },
        { 9, 0, 0, 6, 0, 0, 5, 0, 2 },
        { 4, 0, 0, 0, 1, 0, 2, 0, 0 }, 
        { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 3, 0 }

/*
        { 0, 0, 1,    2, 3, 0,    0, 5, 0 },
        { 0, 0, 3,    0, 0, 0,    0, 1, 0 },
        { 0, 0, 0,    0, 0, 0,    2, 0, 0 },

        { 0, 0, 0,    6, 0, 0,    0, 0, 0 },
        { 0, 1, 0,    4, 0, 0,    0, 0, 3 },
        { 2, 3, 5,    7, 0, 0,    0, 4, 0 },

        { 3, 0, 2,    5, 0, 0,    1, 0, 0 },
        { 0, 0, 0,    1, 4, 0,    0, 0, 2 },
        { 0, 0, 6,    3, 0, 0,    4, 0, 0 }

/*
        { 0, 0, 5,    0, 0, 3,    8, 0, 0 },
        { 2, 0, 0,    0, 0, 0,    0, 6, 0 },
        { 0, 1, 0,    0, 0, 0,    0, 0, 2 },

        { 0, 0, 0,    4, 0, 0,    0, 0, 0 },
        { 8, 0, 0,    0, 0, 0,    1, 9, 0 },
        { 0, 0, 0,    9, 0, 0,    0, 0, 8 },

        { 0, 0, 4,    0, 0, 8,    0, 0, 3 },
        { 0, 0, 0,    7, 0, 6,    0, 5, 0 },
        { 7, 3, 0,    1, 0, 4,    0, 0, 9 }
*/
    };

    // The solution table
    rand int solution[9][9];

    constraint sudoku_init_c {
        foreach (solution[i,j]) {
            // Keep each cell in 1 to 9
            solution[i][j] inside { [1:9] };
            
            // Copy the original Grid into the solution
            if (grid[i][j]) solution[i][j] == grid[i][j];
        }
    }

    // Use Constraints to solve the SUDOKU
    constraint sudoku_solver_csp {
        foreach (solution[i,j]) {

            // each line has a unique value
            foreach (solution[k]) { (k!=j) -> solution[i][j] != solution[i][k]; }

            // each column has a unique value
            foreach (solution[k]) { (k!=i) -> solution[i][j] != solution[k][j]; }

            // each box has a unique value
            foreach (solution[k]) {
                if (( i != (i/3)*3 + k%3) && ( j != (j/3)*3 + k/3))
                    solution[i][j] != solution[(i/3)*3 + k%3][(j/3)*3 + k/3];
            }
        }
    };

    // Print the solution
    function void print();
        for (int ii = 0 ; ii<9 ; ii++) begin 
            if ( ii % 3 == 0 ) $display("|---------|---------|---------|");
            for (int jj = 0 ; jj<9 ; jj++) begin 
                if ( jj % 3 == 0 ) $write("|");
                $write(" %1d ", solution[ii][jj] );
            end
            $display("|");
        end
        $display("|---------|---------|---------|");
    endfunction
endclass



program sudoku;
    sudoku_solver solver = new;
    initial begin
        assert( solver.randomize() );
        solver.print();
    end

endprogram